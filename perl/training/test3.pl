use strict;
use warnings;
use 5.010;

my @a = 0..10;
say scalar( @a );

my @i = ('a','b','c');
for my $j(@i) {
    say $j;
}

