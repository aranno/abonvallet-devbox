use strict;
use warnings;
use 5.010;

my $a = "Hello World";
say '$a: ' . $a;

my @a = ($a,2,3);
say '@a: ' . @a;

say '$a[1]:' . $a[1];
my %b = ( a => 1, b => 2);

my %c = ( 0 => 1, 1 => 2);

say $b{a};

my $d = \@a ;
say $d->[1];

my $g=\%b;
say $g->{a};

my @h = @$d;
my %i = %$g;

say $i{b};
