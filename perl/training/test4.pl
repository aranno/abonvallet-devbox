use strict;
use warnings;
use 5.010;
use YAML;
# say Dump \@ARGV;
my $map = {a => [1,2,3]};
say Dump($map);
my $array = ['un','deux','trois'];
say Dump($array);

my $array_2 = [qw(un deux trois)];
say Dump($array_2);

my @params=qw(un deux trois);
my $first=shift(@params);
say $first;
say Dump(\@params);

sub add {
    my @par = @_ ;
#    say Dump(\@par);
    my $results = 0;
    for my $val(@par) {
        $results += $val;
    }
    return $results;
}

#add('un','deux','trois');
my $sum = add(1,2,3,4,5,6);
say $sum;

say $_ for (1..123);
