#!/bin/bash
for file in *.mp3; do
    mp3date="`id3v2 -l $file | grep 'TDAT' | cut -d' ' -f3 | awk 'BEGIN {FS = "."}{print $3 $2 $1}'`"
    mp3title="`id3v2 -l $file | grep 'TALB' | cut -d':' -f2 | sed 's/^.//' | sed 's/\W/_/g' | iconv -f iso-8859-1 -t utf8`"
    mp3filename=$mp3date'_'$mp3title
    podcast="`echo $file | cut -d'-' -f1`"
    echo $mp3filename
    case $podcast in
        12253)
            id3v2 --TALB "A votre écoute, coûte que coûte" $file;;
        14726)
            id3v2 --TALB "Rendez-vous avec X" $file;;
        18783)
            id3v2 --TALB "Le débat éco" $file;;
    esac
    mv $file $mp3filename.mp3
done
