#! /bin/bash
items=$2
echo "'$items' in parents"
echo -n > /tmp/folders-"$items".csv
function childs {
    /code/ext/gam/gam.py user $1 show filelist query "'$2' in parents" allfields > /tmp/folders-"$items"-tmp.csv
    head -1 /tmp/folders-"$items"-tmp.csv > /tmp/headers-"$items".csv 
    tail -n +2 /tmp/folders-"$items"-tmp.csv >> /tmp/folders-"$items".csv
    folders=$(grep 'application/vnd.google-apps.folder' /tmp/folders-"$items"-tmp.csv | cut -d',' -f2)
    for folder in $folders;
        do
            echo "childs $1 $folder"
            childs $1 $folder
        done
}
childs $1 $2
cat /tmp/headers-"$items".csv /tmp/folders-"$items".csv > /tmp/folders-"$items"-final.csv
cat /tmp/folders-"$items"-final.csv | grep -v 01509265185008993278 > /tmp/folders-"$items"-unshared.csv
/code/ext/gam/gam.py csv /tmp/folders-"$items"-unshared.csv gam user $1 add drivefileacl ~id group staff@linkfluence.com role writer
