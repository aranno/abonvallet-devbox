#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
import json

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

def main():
    ownerOld = 'laurent.hulin@linkfluence.com'
    ownerNew = 'admin-drive@linkfluence.com'

    scopes = 'https://www.googleapis.com/auth/drive'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            'linkfluence-drive-1764d99148d7.json', scopes=scopes)

    delegated_credentials = credentials.create_delegated(ownerOld)
    http = delegated_credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    page_token = None
    files_id = []
    query = "'"+ownerOld+"'"+" in owners"
    while True:
        response = service.files().list( q=query,
                                         corpus='user', spaces='drive',
                                         fields='nextPageToken, files(id, name, owners(emailAddress))',
                                         pageToken=page_token).execute()
        for file in response.get('files', []):
            fileId = file.get('id')
            files_id.append(fileId)
            fileOwner = file.get('owners')[0]['emailAddress']
            fileName = file.get('name').encode('utf-8')
            print('{0} {1} {2}'.format(fileId, fileName, fileOwner))
            update = service.permissions().create(fileId=fileId, body={"emailAddress":ownerNew,
                        "role":"owner", "type":"user"}, transferOwnership=True).execute()
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break;
    print('Moving files')


if __name__ == '__main__':
    main()
