#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
import json

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

# try:
#     import argparse
#     flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
# except ImportError:
#     flags = None

# def delegate()



def main():
    scopes = 'https://www.googleapis.com/auth/drive'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            'linkfluence-drive-1764d99148d7.json', scopes=scopes)
    delegated_credentials = credentials.create_delegated('laurent.hulin@linkfluence.com')
    http = delegated_credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    files_id = []
    page_token = None
    about = service.about().get(fields='user').execute()
    print(about)

    while True:
        response = service.files().list( spaces='drive',
                                         fields='nextPageToken, files(id, name, owners(emailAddress))',
                                         pageToken=page_token).execute()
        for file in response.get('files', []):
            # Process change
            file_name = file.get('name').encode('utf-8')
            # if file.get('owners')['emailAddress'] in 'laurent.hulin@linkfluence.com':
            print('{0} {1} {2}'.format(file.get('id'), file_name, file.get('owners')['emailAddress']))
                #update = service.files().update(fileId='0BxrTbDlC26Mmb3lDTGxtejU4MkE', body={"trashed":"True"}).execute()
            # print('{0} {1} ({2})'.format(file.get('id'), file_name, file.get('sharingUser')))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break;

#     for file_id in files_id:
#         print(file_id)
#         update = service.files().update(fileId=file_id, body={"trashed":"True"}).execute()
#
#     delegated_credentials = credentials.create_delegated('admin-drive@linkfluence.com')
#     http = delegated_credentials.authorize(httplib2.Http())
#     service = discovery.build('drive', 'v3', http=http)
#
#     about = service.about().get(fields='user').execute()
#     print(about)

if __name__ == '__main__':
    main()
