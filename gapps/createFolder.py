#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
import json

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

def main():
    ownerOld = 'laurent.hulin@linkfluence.com'
    ownerNew = 'admin-drive@linkfluence.com'
    # ownerNew = '"%s"'%ownerNew

    scopes = 'https://www.googleapis.com/auth/drive'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            'linkfluence-drive-1764d99148d7.json', scopes=scopes)
    delegated_credentials = credentials.create_delegated(ownerNew)
    http = delegated_credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)
    # Creating Folder ownerOld
    file_metadata = {
        "name" : ownerOld,
        "mimeType" : "application/vnd.google-apps.folder"
        }
    file = service.files().create(body=file_metadata, fields='id').execute()
    folderId = file.get('id')
    print(folderId)

#     for file_id in files_id:
#         folder_id = '0BwwA4oUTeiV1TGRPeTVjaWRDY1E'
#         # Retrieve the existing parents to remove
#         file = drive_service.files().get(fileId=file_id, fields='parents').execute()
#         previous_parents = ",".join(file.get('parents'))
#         # Move the file to the new folder
#         file = drive_service.files().update(fileId=file_id, addParents=folder_id, removeParents=previous_parents, fields='id, parents').execute()
#

if __name__ == '__main__':
    main()
