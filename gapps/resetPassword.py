#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import unicodedata

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

# try:
#     import argparse
#     flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
# except ImportError:
#     flags = None

def main():

    """
    Reset password

    """
    loginUser = sys.argv[1] + '@linkfluence.com'
    newPassword = "ChangeMePlease1%"

    scopes = 'https://www.googleapis.com/auth/admin.directory.user'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
                 'linkfluence-gapps-admin-cert.json', scopes=scopes)
    delegated_credentials = credentials.create_delegated("admin-drive@linkfluence.com")
    http = delegated_credentials.authorize(httplib2.Http())
    service = discovery.build('admin', 'directory_v1', http=http)

    password = service.users().update(userKey=loginUser,body={"password":newPassword,"changePasswordAtNextLogin":"true"}).execute()
    print(newPassword)

if __name__ == '__main__':
    main()

