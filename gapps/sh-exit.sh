#! /bin/bash
user=$1
pass=$(strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 10 | tr -d '\n';)
echo 'Updating password in LDAP'
ldappasswd -H ldap://91.121.30.121 -x -D "cn=admin,dc=linkfluence.net" -y ldap_secret -s $pass "cn=$user,ou=users,dc=linkfluence.net"
email=$(ldapsearch -H ldap://mermoz.linkfluence.net -D cn=admin,dc=linkfluence.net -y ldap_secret -LLL -b "ou=users,dc=linkfluence.net" '(&(objectClass=posixAccount)(uid='"$user"'))' 'mail' | grep 'mail:' | cut -d' ' -f2)
echo 'Updating password in GAPPS & suspend user'
python py-exit-gapps.py $email $pass
echo $user' '$email' '$pass
