#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import unicodedata

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

def main():

    """
    Create GAPPS Account

    """
    # Prepare login & alias from input
    givenName = sys.argv[1]
    familyName = sys.argv[2]
    fullName = givenName + ' ' + familyName
    login = remove_accents(givenName + '.' + familyName)
    primaryEmail = login.lower() + '@linkfluence.com'
    aliasGapps = login.lower() + '@gapps.linkfluence.com'
    print(login.lower())

    # Authorize script for Admin SDK with account admin-drive
    scopes = 'https://www.googleapis.com/auth/admin.directory.user'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
                 'linkfluence-gapps-admin-cert.json', scopes=scopes)
    delegated_credentials = credentials.create_delegated("admin-drive@linkfluence.com")
    http = delegated_credentials.authorize(httplib2.Http())
    # Create service object with credentials
    service = discovery.build('admin', 'directory_v1', http=http)

    # Create account
    account = service.users().insert(body={"primaryEmail":primaryEmail,"name":{"givenName":givenName,"familyName":familyName},"password":"ChangeMePlease1%","changePasswordAtNextLogin":"true","organizations":"Linkfluence","includeInGlobalAddressList":"true"}).execute()

    # Add alias to user
    alias = service.users().aliases().insert(userKey=primaryEmail, body={"alias":aliasGapps}).execute()


if __name__ == '__main__':
    main()

