#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import unicodedata

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools

# try:
#     import argparse
#     flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
# except ImportError:
#     flags = None

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])


def main():

    """
    Create GAPPS Account

    """
    givenName = sys.argv[1]
    familyName = sys.argv[2]
    fullName = givenName + ' ' + familyName
    login = remove_accents(givenName + '.' + familyName)
    print(login.lower())

if __name__ == '__main__':
    main()

