#!/usr/bin/python
#

from __future__ import print_function
import httplib2
import os
import pprint
import sys
import json

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

def main():
    ownerOld = 'laurent.hulin@linkfluence.com'
    ownerNew = 'admin-drive@linkfluence.com'

    scopes = 'https://www.googleapis.com/auth/drive'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            'linkfluence-drive-1764d99148d7.json', scopes=scopes)

    delegated_credentials = credentials.create_delegated(ownerOld)
    http = delegated_credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    page_token = None
    files_id = []
    query = "'"+ownerOld+"'"+" in owners"
    while True:
        response = service.files().list( q=query,
                                         corpus='user', spaces='drive',
                                         fields='nextPageToken, files(id, name, owners(emailAddress))',
                                         pageToken=page_token).execute()
        for file in response.get('files', []):
            fileId = file.get('id')
            files_id.append(fileId)
            fileOwner = file.get('owners')[0]['emailAddress']
            fileName = file.get('name').encode('utf-8')
            print('{0} {1} {2}'.format(fileId, fileName, fileOwner))
            update = service.permissions().create(fileId=fileId, body={"emailAddress":ownerNew,
                        "role":"owner", "type":"user"}, transferOwnership=True).execute()
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break;

    print('Moving files')
    delegated_credentials = credentials.create_delegated(ownerNew)
    http = delegated_credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    # Creating Folder ownerOld
    file_metadata = {
        "name" : ownerOld,
        "mimeType" : "application/vnd.google-apps.folder"
        }
    file = service.files().create(body=file_metadata, fields='id').execute()
    folderId = file.get('id')
    print(folderId)

    for file_id in files_id:
        print(file_id)
        # Retrieve the existing parents to remove
        file = service.files().get(fileId=file_id, fields='parents').execute()
        previous_parents = ",".join(file.get('parents'))
        # Move the file to the new folder
        file = service.files().update(fileId=file_id, addParents=folderId, removeParents=previous_parents, fields='id, parents').execute()


if __name__ == '__main__':
    main()
