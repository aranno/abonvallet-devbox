use Getopt::Long;
use YAML qw(LoadFile);

GetOptions (
    "project=s" => \my $project,
    "file=s"   => \my $file,
    "level=s"  => \my $level,
);

$file = LoadFile $file;

print $_."\n" for @{ $file->{$project}->{$level} };

