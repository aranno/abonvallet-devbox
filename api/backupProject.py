#!/usr/bin/python

import requests
import json
import time
import argparse
import sys
import os

parser = argparse.ArgumentParser(description="Please provide a Project Id to get project data")
parser.add_argument('-P','--project_id',required=True)
args = parser.parse_args()

# parser var
project_id = args.project_id

# web var
url = 'https://radarly.linkfluence.com/1.0/projects/%s.json' % project_id
content_method = 'application/json'
token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFybmF1ZC5ib252YWxsZXRAbGlua2ZsdWVuY2UuY29tIiwidXNlcklkIjoxOSwiZXhwIjoxNTE0NjQzMjE1MDAwLCJsZXZlbCI6InJvb3QiLCJyYW5kIjo0MTU2MjM1MDg1OTExNzMxMn0=.bW5_jxhB7TlxAqH0DtnYOG1DEdkD1BG8Lecd8c2ifVY='
headers= { 'Authorization' : token, 'Content-Type' : content_method }

result = requests.get(url,headers=headers)
try:
	json_unicode=result.json()
        json_focuses=json_unicode["focuses"]
	json_corpus=json_unicode["corpora"]
except ValueError:
	print "projet", project_id, "existe pas"
	sys.exit(1)

# Creating project tree
if not os.path.exists(project_id):
    os.makedirs(project_id)
    os.makedirs('%s/sources' % project_id)
    os.makedirs('%s/focus' % project_id)
    os.makedirs('%s/corpora' % project_id)

# Raw project dump
f = '%s/%s.json' % (project_id, project_id)
project_file = open(f, "w")
project_file.write(json.dumps(json_unicode, indent=2))
project_file.close

# Queries
print "Retrieving queries"
for u in json_focuses:
    query_id = u["id"]
    focusId = str(u["id"])
    query = u["filter"]["query"]
    short = u["filter"]["shortQuery"]
    isSource = u["isSource"]
    if isSource:
	f = '%s/sources/%s.json' % (project_id, query_id)
	project_file = open(f, "w")
	project_file.write(json.dumps(u, indent=2))
	project_file.close
        if query:
            q_convert = query.encode('utf-8').strip()
	    f = '%s/sources/%s_query.sql' % (project_id, query_id)
            project_file = open(f, "w")
            project_file.write(q_convert)
            project_file.close
        if short:
            s_convert = short.encode('utf-8').strip()
            f = '%s/sources/%s_short.sql' % (project_id, query_id)
            project_file = open(f, "w")
            project_file.write(s_convert)
            project_file.close
    else:
	f = '%s/focus/%s.json' % (project_id, query_id)
	project_file = open(f, "w")
	project_file.write(json.dumps(u, indent=2))
	project_file.close
        if query:
            q_convert = query.encode('utf-8').strip()
	    f = '%s/focus/%s_query.sql' % (project_id, query_id)
            project_file = open(f, "w")
            project_file.write(q_convert)
            project_file.close
        if short:
            s_convert = short.encode('utf-8').strip()
            f = '%s/focus/%s_short.sql' % (project_id, query_id)
            project_file = open(f, "w")
            project_file.write(s_convert)
            project_file.close

# Corpora
print "Retrieving corpora"
for u in json_corpus:
    corpus_id = str(u["id"])
    url = 'https://radarly.linkfluence.com/1.0/projects/%s/corpora/%s.json' % (project_id, corpus_id)
    result = requests.get(url,headers=headers)
    json_unicode=result.json()
    #print json.dumps(json_unicode, indent=2)
    f = '%s/corpora/%s.json' % (project_id, corpus_id)
    project_file = open(f, "w")
    project_file.write(json.dumps(json_unicode, indent=2))
    project_file.close

