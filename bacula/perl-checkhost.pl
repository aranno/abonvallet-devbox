#!/usr/bin/perl

use strict;
use Getopt::Long;
use Socket;
use Net::Ping;
use Net::Telnet ();


# Return values:
#   -1  Program error or no host specified
#        0  Success, FD found and responding
#        1  Client alive but FD not listening
#    2  Client not found on network

my $ret = -1;
my ($host, $p, %opts);
GetOptions(\%opts,
           'verbose',
           'retry');

$host = shift || die "No host specified!\n";

if ($opts{verbose})
{
    if ($host =~ /^\d+\.\d+\.\d+\.\d+$/)
    {
        my $ip = inet_aton($host);
        printf("Host $host has name %s\n",
               gethostbyaddr($ip, AF_INET));
    }
    else
    {
        printf("Client $host has address %d.%d.%d.%d\n",
               unpack('C4',
                      (my @addrs = (gethostbyname($host))[4])[0]));
    }
}

$p = Net::Ping->new("icmp");

my $retrycount = ($opts{retry} ? 3 : 1);

while ($retrycount && ($ret != 0))
{
    if ($p->ping($host))
    {
        print "Host $host is alive\n" if ($opts{verbose});
        my $t = new Net::Telnet (Timeout => 10,
                                 Port    => 9102,
                                 Prompt  => '/bash\$ $/',
				 Errmode => 'return');
        if ($t->open($host))
        {
            print "Bacula-FD listening on port 9102\n" if ($opts{verbose});
            $ret = 0;
        }
        else
        {
            print "Bacula-FD not running on host $host\n";
           $ret = 1;
        }
        $t->close;
    }
    else
    {
        print "$p\n";
	print "Client $host not found on network\n";
        $ret = 2;
    }
    $retrycount--;
    sleep(30) if ($retrycount && ($ret != 0));
}
$p->close();

print "Returning value $ret\n" if ($opts{verbose});

exit ($ret);

