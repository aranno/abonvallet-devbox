#! /bin/bash
cd /tmp
mkdir tmp-clone
cd tmp-clone
git clone --bare git@dev:perl-tools
git clone --bare git@dev:perl-tools-core
git clone --bare git@dev:perl-tools-front
mv * /data/containers/gitlab/gitlab/repositories/tools/
sudo chown -R rtgi.admin /data/containers/gitlab/gitlab/repositories/tools/*
sudo chmod 770 /data/containers/gitlab/gitlab/repositories/tools/*
