#! /bin/bash
setfacl -mdefaut:group::rwx -mdefault:group:smb-admi-prive:rwx -mdefault:group:direction:rwx "$1"
setfacl -mgroup:smb-admi-prive:rwx -mgroup:direction:rwx "$1"
